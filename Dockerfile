FROM java:8
ADD ./wiremock-standalone-2.13.0.jar /root
ADD ./mappings /root/mappings
ADD ./__files /root/__files
WORKDIR /root
ENTRYPOINT java -jar ./wiremock-standalone-2.13.0.jar
